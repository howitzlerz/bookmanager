from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http.response import HttpResponseRedirect, JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_exempt
from urllib.request import urlopen, Request;
from urllib.parse import urlencode;
import bs4, json, re;

from book_store.models import Search, Book, Author;

BASE_URL='http://www.amazon.com/s/field-keywords={}';

# Create your views here.
def store(request):
    return render_to_response('index.html', {}, RequestContext(request));

def admin(request):
    return render_to_response('admin.html', {'record_type': 'book'}, RequestContext(request));

def list_record(request, record_type):
    headers=[];
    records=[];
    if record_type=='authors':
        headers=[
            {
                'name': 'Name',
                'size': 20,
            },
            {
                'name': 'Biography',
                'size': 60,
            },
            {
                'name': 'Amazon URL',
                'size': 10,
            }
        ];
        for a in Author.objects.all():
            records.append({
                'name': a.author_name,
                'bio': a.author_bio,
                'link': a.author_link,
            });
    elif record_type=='keywords':
        headers=[
            {
                'name':'Keyword',
                'size': 80,
            },
            {
                'name': 'Count',
                'size': 20,
            }
        ];
        for s in Search.objects.all():
            records.append({
                'keyword': s.search_keyword,
                'count': s.search_count,
            });
    return render_to_response('list.html', {
                                                'record_type': record_type,
                                                'header_list': headers,
                                                'record_list': records,
                                                'record_count': len(headers)+1,
                                            }, RequestContext(request));

def view_record(request, record_type, record_id):
    record_fields=None;
    if record_type=='book' and record_id:
        book_obj=Book.objects.get(book_code=record_id);
        if book_obj:
            #--- SCRAPING START
            #--- Update: Description, Author Link
            if  isNullOrEmpty(book_obj.book_desc):
                _server_response=urlopen(book_obj.book_url);
                _server_soup=bs4.BeautifulSoup(_server_response.read(), 'html5lib');
                _desc=_server_soup.find('div', id='bookDescription_feature_div').find('noscript').text;
                if not isNullOrEmpty(_desc):
                    book_obj.book_desc=_desc.strip();
                    book_obj.save();
                _author=_server_soup.findAll('a', attrs={'class':'contributorNameID'}, href=True);
                for a in _author:
                    _my_author, created=Author.objects.get_or_create(author_name=a.text);
                    _my_author.author_link='http://www.amazon.com{}'.format(a['href']);
                    _my_author.save();
            #--- SCRAPING END
            book_authors=[];
            for a in book_obj.book_author.all():
                book_authors.append(a.__str__());
            book_search=[];
            for s in book_obj.book_search.all():
                book_search.append(s.__str__());
            record_fields=[
                {
                    'name':'Title',
                    'value': book_obj.book_title,
                    'type': 'text',
                },
                {
                    'name': 'Description',
                    'value': book_obj.book_desc,
                    'type': 'textarea',
                },
                {
                    'name':'Rating',
                    'value': book_obj.book_rating,
                    'type': 'text',
                },
                {
                    'name':'Price',
                    'value': book_obj.book_price,
                    'type': 'text',
                },
                {
                    'name':'Image',
                    'value': book_obj.book_img,
                    'type': 'text',
                },
                {
                    'name':'URL',
                    'value': book_obj.book_url,
                    'type': 'text',
                },
                {
                    'name': 'Author',
                    'value': book_authors,
                    'type': 'list',
                },
                {
                    'name': 'Tags',
                    'value': book_search,
                    'type': 'list',
                },
            ];
    elif record_type=='author' and record_id:
        author_obj=Author.objects.get(author_name=record_id);
        if author_obj:
            #--- SCRAPING START
            #--- Update: Author Bio
            print(author_obj.author_link);
            if isNullOrEmpty(author_obj.author_bio) and not isNullOrEmpty(author_obj.author_link):
                _server_response=urlopen(author_obj.author_link);
                _server_soup=bs4.BeautifulSoup(_server_response.read(), 'html5lib');
                _author_bio=_server_soup.find('div', id='ap-bio').text;
                if not isNullOrEmpty(_author_bio):
                    _author_bio=_author_bio.replace('See more', '').strip();
                    author_obj.author_bio=_author_bio;
                    author_obj.save();
            #--- SCRAPING END
            record_fields=[
                {
                    'name': 'Name',
                    'value': author_obj.author_name,
                    'type':'text'
                },
                {
                    'name': 'Bio',
                    'value': author_obj.author_bio,
                    'type': 'textarea',
                },
                {
                    'name': 'Link',
                    'value': author_obj.author_link,
                    'type': 'text',
                },
            ];
    return render_to_response('view.html', {
                                            'record_field': record_fields,
                                            'record_type': record_type,
                                            'record_id': record_id
                                            }, RequestContext(request));
    
@ensure_csrf_cookie
def update_record(request, record_type, record_id):
    record_fields=None;
    if record_type=='book' and record_id:
        book_obj=Book.objects.get(book_code=record_id);
        authors_list=[];
        search_list=[];
        for a in Author.objects.all():
            authors_list.append({
                'id': a.author_id,
                'name': a.author_name,
            });
        for s in Search.objects.all():
            search_list.append({
                'id': s.search_id,
                'name': s.search_keyword,
            });
        if book_obj:
            book_authors=[];
            for a in book_obj.book_author.all():
                book_authors.append(a.__str__());
            book_search=[];
            for s in book_obj.book_search.all():
                book_search.append(s.__str__());
            record_fields=[
                {
                    'name':'Title',
                    'value': book_obj.book_title,
                    'type': 'text',
                    'id': 'book_title',
                },
                {
                    'name': 'Description',
                    'value': book_obj.book_desc,
                    'type': 'textarea',
                    'id': 'book_desc',
                },
                {
                    'name':'Rating',
                    'value': book_obj.book_rating,
                    'type': 'text',
                    'id':'book_rating',
                },
                {
                    'name':'Price',
                    'value': book_obj.book_price,
                    'type': 'text',
                    'id':'book_price',
                },
                {
                    'name':'Image',
                    'value': book_obj.book_img,
                    'type': 'text',
                    'id': 'book_image',
                },
                {
                    'name':'URL',
                    'value': book_obj.book_url,
                    'type': 'text',
                    'id':'book_link',
                },
                {
                    'name': 'Author',
                    'value': book_authors,
                    'type': 'multilist',
                    'choices': authors_list,
                    'id':'book_author',
                },
                {
                    'name': 'Tags',
                    'value': book_search,
                    'type': 'multilist',
                    'choices': search_list,
                    'id':'book_search',
                },
            ];
    elif record_type=='author' and record_id:
        author_obj=Author.objects.get(author_name=record_id);
        if author_obj:
            record_fields=[
                {
                    'name': 'Name',
                    'value': author_obj.author_name,
                    'type':'text',
                    'id': 'author_name',
                },
                {
                    'name': 'Bio',
                    'value': author_obj.author_bio,
                    'type': 'textarea',
                    'id': 'author_bio',
                },
                {
                    'name': 'Link',
                    'value': author_obj.author_link,
                    'type': 'text',
                    'id': 'author_link',
                },
            ];
    return render_to_response('update.html', {
                                                'record_field': record_fields,
                                                'record_type': record_type,
                                                'record_id': record_id,
                                            }, RequestContext(request));

@ensure_csrf_cookie
def save_record(request, record_type, record_id):
    if record_type=='book' and record_id:
        book_obj=Book.objects.get(book_code=record_id);
        if book_obj:
            #Params
            param_book_title=request.POST.get('book_title');
            param_book_desc=request.POST.get('book_desc');
            param_book_rating=request.POST.get('book_rating');
            param_book_price=request.POST.get('book_price');
            param_book_image=request.POST.get('book_image');
            param_book_link=request.POST.get('book_link');
            #Book
            book_obj.book_title=param_book_title;
            book_obj.book_desc=param_book_desc;
            book_obj.book_rating=param_book_rating;
            book_obj.book_price=param_book_price;
            book_obj.book_img=param_book_image;
            book_obj.book_link=param_book_link;
            book_obj.book_author.clear();
            book_obj.book_search.clear();
            book_obj.save();
            #Authors
            param_book_authors=request.POST.getlist('book_author');
            for a in param_book_authors:
                book_obj.book_author.add(Author.objects.get(pk=a));
            #Kewwords
            param_book_keywords=request.POST.getlist('book_search');
            for s in param_book_keywords:
                book_obj.book_search.add(Search.objects.get(pk=s));
            return HttpResponseRedirect('/admin/view/book/{}'.format(book_obj.book_code));
    elif record_type=='author' and record_id:
        author_obj=Author.objects.get(author_name=record_id);
        if author_obj:
            #Params
            param_author_name=request.POST.get('author_name');
            param_author_bio=request.POST.get('author_bio');
            param_author_link=request.POST.get('author_link');
            #Author
            author_obj.author_name=param_author_name;
            author_obj.author_bio=param_author_bio;
            author_obj.author_link=param_author_link;
            author_obj.save();
            return HttpResponseRedirect('/admin/view/author/{}'.format(author_obj.author_name));
    return HttpResponse('Hi there!');

@ensure_csrf_cookie    
def delete_record(request, record_type, record_id):
    if record_type=='book' and record_id:
        book_obj=Book.objects.get(book_code=record_id);
        if book_obj:
            book_obj.delete();
        return HttpResponseRedirect('/admin/');
    elif record_type=='author' and record_id:
        author_obj=Author.objects.get(author_name=record_id);
        if author_obj:
            author_obj.delete();
        return HttpResponseRedirect('/admin/list/authors');
    elif record_type=='keyword' and record_id:
        keyword_obj=Search.objects.get(search_id=record_id);
        if keyword_obj:
            keyword_obj.delete();
        return HttpResponseRedirect('/admin/list/keywords');

@ensure_csrf_cookie
def search_record(request, keyword=''):
    result_list=[];
    search_list=Search.objects.filter(search_keyword__contains=keyword);
    for s in search_list:
        s.search_count=s.search_count+1;
        s.save();
        book_list=Book.objects.filter(book_search=s);
        for b in book_list:
            author_list=[];
            for a in b.book_author.all():
                author_list.append(a.__str__());
            result_list.append({
                'title': b.book_title,
                'id': b.book_code,
                'rating': b.book_rating,
                'price': b.book_price,
                'image': b.book_img,
                'link': b.book_url,
                'author': author_list,
                'desc': b.book_desc,
            });
    return render_to_response('search.html', {'search_result': result_list, 'keyword': keyword}, RequestContext(request));
    
@ensure_csrf_cookie
def search_item(request, book_id=''):
    result_item=Book.objects.get(book_code=book_id);
    author_items=[];
    if result_item:
        #--- SCRAPING START
        #--- Update: Description, Author Link
        if  isNullOrEmpty(result_item.book_desc):
            _server_response=urlopen(result_item.book_url);
            _server_soup=bs4.BeautifulSoup(_server_response.read(), 'html5lib');
            _desc=_server_soup.find('div', id='bookDescription_feature_div');
            if _desc:
                _desc=_desc.find('noscript').text;
                if not isNullOrEmpty(_desc):
                    result_item.book_desc=_desc.strip();
                    result_item.save();
        #--- SCRAPING END
        author_items=result_item.book_author.all();
    return render_to_response('item.html', {'result': result_item, 'authors': author_items}, RequestContext(request));
        
@csrf_exempt
def service_book_add(request):
    param_book_id=request.POST.get('id');
    param_book_title=request.POST.get('title');
    param_book_price=request.POST.get('price');
    param_book_link=request.POST.get('link');
    param_book_image=request.POST.get('image');
    param_book_rating=request.POST.get('rating');
    param_book_author=request.POST.getlist('author[]');
    param_book_search=request.POST.get('search');
    book_obj=Book(
        book_code=param_book_id,
        book_title=param_book_title,
        book_rating=param_book_rating,
        book_price=param_book_price,
        book_img=param_book_image,
        book_url=param_book_link
    );
    try:
        book_obj.save();
        for a in param_book_author:
            author_obj, created=Author.objects.get_or_create(author_name=a);
            author_obj.save();
            book_obj.book_author.add(author_obj);
        if not isNullOrEmpty(param_book_search):
            book_obj.book_search.add(Search.objects.get(pk=param_book_search));
        return JsonResponse({'success': True});
    except Exception as e:
        print(e);
        return JsonResponse({'success': False});

@csrf_exempt
def service_keyword_search(request):
    param_keyword=request.POST.get('keyword');
    search_result=[];
    if isNullOrEmpty(param_keyword):
        search_obj=Search.objects.order_by('-search_count');
    else:
        search_obj=Search.objects.filter(search_keyword__contains=param_keyword);
    if search_obj:
        search_obj=search_obj[0:10];
    for s in search_obj:
        search_result.append(s.search_keyword);
    return JsonResponse({'result': search_result});
        
@ensure_csrf_cookie
def service_book_list(request, order_type=None):
    book_list=[];
    books_obj=Book.objects.all();
    if order_type=='new':
        books_obj=books_obj.order_by('-book_datetime');
    elif order_type=='sale':
        books_obj=books_obj.order_by('book_price');
    for b in books_obj:
        book_item={
            'id': b.book_code,
            'title': b.book_title,
            'rating': b.book_rating,
            'price': b.book_price,
            'image': b.book_img,
            'link': b.book_url,
        };
        book_item['author']=[];
        for a in b.book_author.all():
            book_item['author'].append(a.author_name);
        book_list.append(book_item);
    return JsonResponse({'result': book_list});

@csrf_exempt
def service_book_delete(request):
    param_book_codes=request.POST.getlist('book_codes[]');
    book_deleted=[];
    for code in param_book_codes:
        try:
            book_obj=Book.objects.get(book_code=code);
            book_obj.delete();
            book_deleted.append(code);
        except Exception as e:
            print(e);
    return JsonResponse({'book_code_deleted': book_deleted});

@ensure_csrf_cookie
def service_scrape_search(request, keyword=''):
    keyword=keyword.lower();
    _key_obj, created=Search.objects.get_or_create(search_keyword=keyword);
    #_key_obj.search_count=_key_obj.search_count+1;
    _key_obj.save();
    book_list=[];
    _server_response=urlopen(BASE_URL.format(keyword));
    _server_soup=bs4.BeautifulSoup(_server_response.read(), 'html5lib');
    _result_list=_server_soup.findAll('li', attrs={'class':'s-result-item'});
    for result in _result_list:
        _book_title=result.find('h2',attrs={'class': 's-access-title'});
        #--- BOOK TITLE ISNT EMPTY
        if not isNullOrEmpty(_book_title):
            book_item={
                'title': _book_title.text,
                'image': result.find('img', attrs={'class': 's-access-image'})['src'],
            };
            _link=result.find('a', attrs={'class': 's-access-detail-page'}, href=True);
            if _link:
                book_item['link']=_link['href'];
            else:
                book_item['link']='';
            _price=result.find('span', attrs={'class': 's-price'});
            if _price:
                book_item['price']=_price.text;
            else:
                book_item['price']='$0.00';
            if not isNullOrEmpty(book_item['link']):
                book_item['id']=re.search(r'dp/[A-Za-z0-9]+', book_item['link']).group().replace('dp/', '');
                _rating=result.find('span', attrs={'name': book_item['id']});
                if not isNullOrEmpty(_rating):
                    book_item['rating']=_rating.text.strip();
            authors=result.find('a', attrs={'class': 's-access-detail-page'}).parent.find('div');
            if authors:
                authors=authors.findAll('span');
                book_item['author']=[];
                #book_item['author_link']=[];
                book_item['search']=_key_obj.search_id;
                for a in authors:
                    if a.text.strip()!='by':
                        book_item['author'].append(a.text);
                book_list.append(book_item);
        #--- BOOK TITLE ISNT EMPTY
    return JsonResponse({'result': book_list, 'count': len(book_list)});
    
def isNullOrEmpty(data):
    return (data==None or data=='');