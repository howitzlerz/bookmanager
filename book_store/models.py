from django.db import models

# Create your models here.
class Search(models.Model):
    search_id=models.AutoField(primary_key=True, unique=True, verbose_name='Search ID');
    search_keyword=models.CharField(max_length=32, verbose_name='Keyword');
    search_count=models.IntegerField(verbose_name='Count', default=0);
    search_datetime=models.DateTimeField(auto_now=True, verbose_name='Date/Time');
    def __str__(self):
        return self.search_keyword;

class Author(models.Model):
    author_id=models.AutoField(primary_key=True, unique=True, verbose_name='Author ID');
    author_name=models.CharField(max_length=64, verbose_name='Name');
    author_bio=models.TextField(verbose_name='Bio', null=True);
    author_link=models.TextField(verbose_name='Link', null=True);
    author_datetime=models.DateTimeField(auto_now=True, verbose_name='Date/Time');
    def __str__(self):
        return self.author_name;

class Book(models.Model):
    book_id=models.AutoField(primary_key=True, unique=True, verbose_name='Book ID');
    book_code=models.CharField(max_length=32, verbose_name='Code');
    book_title=models.CharField(max_length=120, verbose_name='Title');
    book_desc=models.TextField(verbose_name='Description', null=True, blank=True);
    book_rating=models.CharField(max_length=32, verbose_name='Rating');
    book_price=models.CharField(max_length=12, verbose_name='Price');
    book_img=models.TextField(verbose_name='Image');
    book_url=models.TextField(verbose_name='URL');
    book_author=models.ManyToManyField(Author, verbose_name='Author', null=True);
    book_search=models.ManyToManyField(Search, verbose_name='Search', null=True);
    book_datetime=models.DateTimeField(auto_now=True, verbose_name='Date/Time');
    def __str__(self):
        return self.book_title;