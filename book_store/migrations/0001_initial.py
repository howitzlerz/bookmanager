# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('author_id', models.AutoField(primary_key=True, verbose_name='Author ID', unique=True, serialize=False)),
                ('author_name', models.CharField(verbose_name='Name', max_length=64)),
                ('author_bio', models.TextField(verbose_name='Bio', null=True)),
                ('author_link', models.TextField(verbose_name='Link', null=True)),
                ('author_datetime', models.DateTimeField(verbose_name='Date/Time', auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Book',
            fields=[
                ('book_id', models.AutoField(primary_key=True, verbose_name='Book ID', unique=True, serialize=False)),
                ('book_code', models.CharField(verbose_name='Code', max_length=32)),
                ('book_title', models.CharField(verbose_name='Title', max_length=120)),
                ('book_desc', models.TextField(verbose_name='Description', blank=True, null=True)),
                ('book_rating', models.CharField(verbose_name='Rating', max_length=32)),
                ('book_price', models.CharField(verbose_name='Price', max_length=12)),
                ('book_img', models.TextField(verbose_name='Image')),
                ('book_url', models.TextField(verbose_name='URL')),
                ('book_datetime', models.DateTimeField(verbose_name='Date/Time', auto_now=True)),
                ('book_author', models.ManyToManyField(verbose_name='Author', null=True, to='book_store.Author')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Search',
            fields=[
                ('search_id', models.AutoField(primary_key=True, verbose_name='Search ID', unique=True, serialize=False)),
                ('search_keyword', models.CharField(verbose_name='Keyword', max_length=32)),
                ('search_count', models.IntegerField(default=0, verbose_name='Count')),
                ('search_datetime', models.DateTimeField(verbose_name='Date/Time', auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='book',
            name='book_search',
            field=models.ManyToManyField(verbose_name='Search', null=True, to='book_store.Search'),
            preserve_default=True,
        ),
    ]
