# Book Manager #
## Overview ##
***
This is a small and fun project that aims to:


1. Scrape and parse Books from Amazon


2. Store it in a database


3. Build an API server with a RESTful interface


4. Present data in user friendly interface

## Prerequisite ##
Python 3.0 or higher 

## Installation ##
***
1. Clone this source code and open your command prompt/console and point to the current directory of this code.

2. Activate virtualenv and start your server by typing the following set of commands:


```batch
#For windows
venv/Scripts/activate
#For Mac/Linux
source venv/bin/activtae

python manage.py runserver
```

Open your browser to: http://127.0.0.1:8000/ and enjoy!



## Front End Stack ##
***
1. ReactJS - This provide interactivity between the user and the record stored in the system. 

2. jQuery/AJAX - It is a small javascript library that enables fast and lightweight request to RESTful services.

4. HTML5/CSS - UI Building blocks.

## Back End Stacks ##
***
1. SQLite/ Postgres - Database of the records used by the system



## Server ##
***
1. Python/ Django - is a high-level Python Web framework that encourages rapid development and clean, pragmatic design. Built by experienced developers, it takes care of much of the hassle of Web development, so you can focus on writing your app without needing to reinvent the wheel. It's free and open source.

## Scope and Limitation ##
Due to the project requirement and time constrains, this project has not been fully completed. 

The following are the scope and limitation of the project:

1. Adding book to the store can only be done from the admin role. Go to http://127.0.0.1:8000/admin and type in
 the book you wish to look for then hover and click the Add to Store button. The loading time of results may take up to 30 seconds depending on your connection speed and loading time of amazon page being scraped.

2. Author's bio and Book's description will be parsed when the book item has been loaded or open from the admin role.

3. Checkout module is only up to the point where you can add/remove item in your cart. Moving to other page, will automatically lose your cart items.

4. Record's Data Validation has not been implemented yet.

5. There will be times where a page/ searching a book takes longer than usual. Just refresh the page as sometimes the Amazon page's denies access in protection to possible DOS attacks.

6. Using the admin role, the following are the information parsed from Amazon:

* Title
* Rating
* Price
* Book Image URL
* Book's Amazon URL
* Author's Name