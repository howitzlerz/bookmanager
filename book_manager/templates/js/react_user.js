var CART_ITEMS=[], CART_OBJ=null;
var CART_QTYS=[], CART_PRICES=[], CART_TOTAL=0.00;

var CollectionLabel=React.createClass({
    render: function() {
        return (
            <div className="book_collection_label">{this.props.label}</div>
        );
    }
});

var CollectionItem=React.createClass({
    cartHandler: function(evt) {
        evt.preventDefault();
        if (this.exists()) {
            alert('This item has been added to your cart.');
        }
        else {
            CART_ITEMS.push(this.props.book);
            if (CART_OBJ) {
                CART_OBJ.setState({data: CART_ITEMS});
            }
        }
    },
    exists: function() {
        for (var i=0;i<CART_ITEMS.length;i++) {
            if (CART_ITEMS[i].id==this.props.book.id) {
                return true;
            }
        }
        return false;
    },
    render: function() {
        return (
            <div className="collection_item">
                <div className="collection_img">
                    <a href={ '/book/' + this.props.book.id + '/' }>
                        <img src={this.props.book.image} />
                    </a>
                </div>
                <div className="collection_title">
                    <a href={ '/book/' + this.props.book.id + '/' }>{this.props.book.title}</a>
                </div>
                <div className="collection_author">by {this.props.book.author}</div>
                <div className="collection_price">Price: {this.props.book.price}</div>
                <div className="collection_action">
                    <a href="#" onClick={this.cartHandler}>
                        <div className="action_item">
                            <div className="action_cart"></div>
                            <span className="action_label">Add to cart</span>
                        </div>
                    </a>
                </div>
            </div>
        );
    }
});

var Collection=React.createClass({
    ajax_server: null,
    getInitialState: function() {
        return ({data: []});
    },
    componentWillMount: function() {
        var _this=this;
        _this.ajax_server=jQuery.ajax({
            url: this.props.url,
            method: 'GET',
            type: 'GET',
            dataType: 'json',
            success: function(ajax_result) {
                _this.setState({data: ajax_result.result});
            }
        });
    },
    componentWillUnmount: function() {
        if (this.ajax_server) {
            this.ajax_server.abort();
        }
        this.ajax_server=null;
    },
    render: function() {
        return (
            <div className="book_store_collection">
                <CollectionLabel label={this.props.label}/>
                <div className="book_collection_container">
                    {this.state.data.slice(0, this.props.max).map(function(i,e) {
                        return <CollectionItem book={i} key={e}/>;
                    })}
                    <div className="clearB"></div>
                </div>
            </div>
        );
    }
});

ReactDOM.render(<Collection label="What's New" url="/service/book/list/new" max="6"/>, document.getElementById('collection_whats_new'));
ReactDOM.render(<Collection label="Sales" url="/service/book/list/sale" max="6"/>, document.getElementById('collection_sale'));

var SearchItem=React.createClass({
    render: function() {
        return (
            <li>
                <a href={'/search/book/' + this.props.keyword + '/'} onClick={this.props.select}>{this.props.keyword}</a>
            </li>
        );
    }
});

var SearchBar=React.createClass({
    ajax_server: null,
    getInitialState: function() {
        return ({init_data: [], data: []});
    },
    searchHandler: function(evt) {
        var _keyword=evt.target.value;
        var _this=this;
        if (_keyword.length>=3) {
            _this.ajax_server=jQuery.ajax({
                url: '/service/keyword/search/',
                method: 'POST',
                type: 'POST',
                data: {keyword: _keyword},
                dataType: 'json',
                success: function(ajax_result) {
                    _this.setState({data: ajax_result.result});
                }
            });
        }
        else {
            _this.setState({data: _this.state.init_data});
        }
    },
    componentWillMount: function() {
        var _this=this;
        _this.ajax_server=jQuery.ajax({
            url: '/service/keyword/search/',
            method: 'POST',
            type: 'POST',
            dataType: 'json',
            success: function(ajax_result) {
                _this.setState({data: ajax_result.result, init_data: ajax_result.result});
            }
        });
    },
    componentWillUnmount: function() {
        if (this.ajax_server) {
            this.ajax_server.abort();
        }
        this.ajax_server=null;
    },
    render: function() {
        var _this=this;
        return (
            <div className="book_store_search">
                <div className="book_search_label">Top Searches</div>
                <div className="book_search_box">
                    <input type="text" className="book_search_txt" placeholder="Enter keyword" onKeyUp={this.searchHandler}/>
                    <div className="book_search_btn">Search</div>
                    <div className="clearB"></div>
                </div>
                <div className="book_search_suggestion">
                    <ul>
                        {this.state.data.map(function(i,e) {
                            return <SearchItem keyword={i} key={e}/>
                        })}
                    </ul>
                </div>
            </div>
        );
    }
});

var CartItem=React.createClass({
    getInitialState: function() {
        return ({qty: 1});
    },
    componentDidMount: function() {
        CART_QTYS[this.props.index]=1;
        CART_PRICES[this.props.index]=parseFloat(this.props.book.price.replace(/[^0-9.]/gi, ''));
        CART_TOTAL+=CART_PRICES[this.props.index];
        CART_OBJ.setState({total: CART_TOTAL});
    },
    componentWillUnmount: function() {
        CART_TOTAL-=(CART_QTYS[this.props.index] * CART_PRICES[this.props.index]);
        CART_QTYS.splice(this.props.index, 1);
        CART_PRICES.splice(this.props.index, 1);
        CART_OBJ.setState({total: CART_TOTAL});
    },
    changeHandler: function(evt) {
        var _qty=evt.target.value;
        CART_QTYS[this.props.index]=parseInt(_qty);
        var _total=0.00;
        for (var i=0;i<CART_QTYS.length;i++) {
            _total+=(CART_QTYS[i] * CART_PRICES[i]);
        }
        CART_TOTAL=_total;
        CART_OBJ.setState({total: CART_TOTAL});
        
        this.setState({qty: _qty});
    },
    clickHandler: function(evt) {
        evt.preventDefault();
        CART_ITEMS.splice(this.props.index, 1);
        CART_OBJ.setState({data: CART_ITEMS});
    },
    render: function() {
        return (
            <div className="collection_cart_item">
                <div className="cart_item_left">
                    <img src={this.props.book.image} />
                </div>
                <div className="cart_item_right">
                    <div className="cart_item_name">{this.props.book.title}</div>
                    <div className="cart_item_line">
                        <input type="text" className="cart_item_qty" value={this.state.qty} onChange={this.changeHandler}/>
                        <span className="cart_item_price">x {this.props.book.price}</span>
                        <a href="#" className="action_delete" onClick={this.clickHandler}>Remove</a>
                    </div>
                </div>
                <div className="clearB"></div>
            </div>
        );
    }
});

var Cart=React.createClass({
    getInitialState: function() {
        return ({data: [], total: 0.00});
    },
    getTotal: function() {
        var x=this.state.total;
        var y=x;
        if (y.toString().indexOf('.')==-1) {
            y=x.toFixed(2);
        }
        var parts=y.toString().split('.');
        parts[0]=parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (parts[1]!=null && parts[1].length==1) {
            parts[1]+='0';
        }
        else if (parts[1]!=null && parts[1].length>2) {
            parts[1]=parts[1].substring(0,2);
        }
        return parts.join(".");
    },
    render: function() {
        if (this.state.data.length==0) {
            return (
                <div className="collection_cart_container">
                    <div className="collection_cart_label">My cart</div>
                    <div className="collection_cart_list">
                        <div className="collection_cart_empty">
                            <img src="/templates/images/cart_empty.png" />
                            <span>You cart is empty</span>
                        </div>
                    </div>
                </div>
            );
        }
        else {
            var _this=this;
            return (
                <div className="collection_cart_container">
                    <div className="collection_cart_label">My cart</div>
                    <div className="collection_cart_list">
                    {this.state.data.map(function(i,e) {
                         return <CartItem book={i} key={e} ref={i.id} index={e}/>;
                    })}
                    </div>
                    <div className="collection_cart_total">
                        <a href="#" className="action_update">Proceed to checkout</a>
                        <span className="cart_total_label">Total: </span>
                        <span className="cart_total_value">${this.getTotal()}</span>
                    </div>
                </div>
            );
        }
    }
});

ReactDOM.render(<SearchBar />, document.getElementById('collection_search'));
CART_OBJ=ReactDOM.render(<Cart />, document.getElementById('collection_cart'));