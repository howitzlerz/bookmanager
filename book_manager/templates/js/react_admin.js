var BOOK_LIST=[];
var SEARCH_LIST=[];

var BOOK_TABLE_OBJ=null;
var SEARCH_TABLE_OBJ=null;

var BookLabel=React.createClass({
    render: function() {
        return(
            <div className="book_list_label">{this.props.label}</div>
        );
    }
});
var BookItem=React.createClass({
    getURL: function() {
        return '/admin/view/book/'+this.props.book.id;
    },
    getAuthorURL: function(author_name) {
        return '/admin/view/author/'+author_name;
    },
    render: function() {
        return (
            <tr>
                <td width="10%">
                    <input type="checkbox" className="book_list_selector" id={this.props.book.id}/>
                </td>
                <td width="35%">
                    <a href={this.getURL()} target="_blank">{this.props.book.title}</a>
                </td>
                <td width="25%">
                    {this.props.book.author.map(function(i,e) {
                        return <div key={e}><a target="_blank" href={ "/admin/view/author/"+i+"/" }>{i}</a></div>;
                    })}
                </td>
                <td width="15%">{this.props.book.rating}</td>
                <td width="15%">{this.props.book.price}</td>
            </tr>
        );
    }
});
var BookList=React.createClass({
    ajax_server: null,
    getInitialState: function() {
        return {data: BOOK_LIST};
    },
    componentDidMount: function() {
        BOOK_TABLE_OBJ=this;
    },
    componentWillMount: function() {
        var _this=this;
        _this.ajax_server=jQuery.ajax({
            url: '/admin/book/list/',
            type: 'GET',
            method: 'GET',
            dataType: 'json',
            success: function(ajax_result) {
                if (BOOK_TABLE_OBJ) {
                    BOOK_LIST=ajax_result.result;
                    BOOK_TABLE_OBJ.setState({data: BOOK_LIST});
                }
                _this.ajax_server=null;
            }
        });
    },
    render: function() {
        if (this.state.data.length==0) {
            return (
                <table width="100%" cellPadding="0" cellSpacing="0" className="book_list_table">
                    <tbody>
                        <tr>
                            <th width="10%">Select</th>
                            <th width="35%">Title</th>
                            <th width="25%">Author</th>
                            <th width="15%">Rating</th>
                            <th width="15%">Price</th>
                        </tr>
                        <tr>
                            <td colSpan="5" align="center">No record to display.</td>
                        </tr>
                    </tbody>
                </table>
            );
        }
        else {
            return (
                <table width="100%" cellPadding="0" cellSpacing="0" className="book_list_table">
                    <tbody>
                        <tr>
                            <th width="10%">Select</th>
                            <th width="35%">Title</th>
                            <th width="25%">Author</th>
                            <th width="15%">Rating</th>
                            <th width="15%">Price</th>
                        </tr>
                        {this.state.data.map(function(i, e) {
                            return <BookItem key={e} book={i}/>;
                        })}
                    </tbody>
                </table>
            );
        }
    }
});

var BookAction=React.createClass({
    ajax_server: null,
    markAllHandler: function(evt) {
        evt.preventDefault();
        jQuery('.book_list_selector').attr('checked', true);
        
    },
    unmarkAllHandler: function(evt) {
        evt.preventDefault();
        jQuery('.book_list_selector').attr('checked', false);
    },
    deleteHandler: function(evt) {
        evt.preventDefault();
        var book_codes=[];
        jQuery('.book_list_selector:checked').each(function(i, e) {
            book_codes.push(jQuery(this).attr('id'));
        });
        console.log(book_codes);
        if (book_codes.length==0) {
            alert('No selected record to delete.');
        }
        else if (confirm('This will permanently delete these selected records. Are you sure you want to continue?')) {
            var _this=this;
            _this.ajax_server=jQuery.ajax({
                url: '/admin/book/delete/',
                method: 'POST',
                type: 'POST',
                dataType: 'json',
                data: {'book_codes': book_codes},
                success: function(ajax_result) {
                    var book_code_deleted=ajax_result.book_code_deleted;
                    for (var i=0;i<book_code_deleted.length;i++) {
                        for (var j=0;j<BOOK_LIST.length;j++) {
                            if (BOOK_LIST[j].id==book_code_deleted[i]) {
                                BOOK_LIST.splice(j, 1);
                            }
                        }
                    }
                    BOOK_TABLE_OBJ.setState({data: BOOK_LIST});
                }
            });
        }
    },
    componentWillUnmount: function() {
        if (this.ajax_server) {
            this.ajax_server.abort();
        }
        this.ajax_server=null;
    },
    render: function() {
        return (
            <div className="book_store_action">
                <a href="#" className="action_markAll" onClick={this.markAllHandler}>Mark All</a>
                <a href="#" className="action_unmarkAll" onClick={this.unmarkAllHandler}>Unmark All</a>
                <a href="#" className="action_delete" onClick={this.deleteHandler}>Delete</a>
            </div>
        );
    }
});

var BookTable=React.createClass({
    render: function() {
        return (
            <div className="book_store_list">
                <BookLabel label={this.props.label}/>
                <BookList />
                <BookAction />
            </div>
        );
    }
});
ReactDOM.render(<BookTable label="Books"/>, document.getElementById('book_container_left'));

var SearchLabel=React.createClass({
    render: function() {
        return (
            <div className="book_search_label">{this.props.label}</div>
        );
    }
});

var SearchBar=React.createClass({
    ajax_server: null,
    submitHandler: function(evt) {
        evt.preventDefault();
        var _loader=document.getElementById('search_icon');
        if (_loader) {
            _loader.setAttribute('src', '/templates/images/ajax_loader.gif');
        }
        var _keyword=document.getElementById('search_keyword').value;
        if (_keyword) {
            var _this=this;
            _this.ajax_server=jQuery.ajax({
                url: this.props.search+_keyword,
                type: 'GET',
                method: 'GET',
                dataType: 'json',
                success: function(ajax_result) {
                    if (SEARCH_TABLE_OBJ) {
                        SEARCH_TABLE_OBJ.setState({data: ajax_result.result, hasLoaded: true});
                        console.log(ajax_result.result);
                    }
                    _this.ajax_server=null;
                    if (_loader) {
                        _loader.setAttribute('src', '/templates/images/search_action_focus.png');
                    }
                }
            });
        }
    },
    componentWillUnmount: function() {
        if (this.ajax_server) {
            this.ajax_server.abort();
        }
        this.ajax_server=null;
    },
    render: function() {
        return (
            <div className="admin_search_box">
                <form method="GET" onSubmit={this.submitHandler}>
                    <input type="text" placeholder="Enter keyword" id="search_keyword"/>
                    <img src="/templates/images/search_action_focus.png" className="admin_search_btn" id="search_icon"/>
                </form>
            </div>
        );
    }
});

var SearchItem=React.createClass({
    ajax_server: null,
    addToStore: function(evt) {
        evt.preventDefault();
        if (!this.exists()) {
            var _this=this;
            _this.ajax_server=jQuery.ajax({
                url: '/admin/book/add/',
                method: 'POST',
                type: 'POST',
                data: this.props.book,
                dataType: 'json',
                success: function(ajax_result) {
                    if (ajax_result.success) {
                        BOOK_LIST.push(_this.props.book);
                        BOOK_TABLE_OBJ.setState({data: BOOK_LIST});
                    }
                }
            });
        }
        else {
            alert('Item already exists in the store.');
        }
    },
    componentWillUnmount: function() {
        if (this.ajax_server) {
            this.ajax_server.abort();
        }
        this.ajax_server=null;
    },
    exists: function() {
        for (var i=0;i<BOOK_LIST.length;i++) {
            if (BOOK_LIST[i].id==this.props.book.id) {
                return true;
            }
        }
        return false;
    },
    render: function() {
        return (
            <div className="search_item">
                <div className="search_item_left">
                    <a href={this.props.book.link} target="_blank">
                        <img src={this.props.book.image} />
                    </a>
                </div>
                <div className="search_item_right">
                    <div className="search_item_title">
                        <a href={this.props.book.link} target="_blank">{this.props.book.title}</a>
                    </div>
                    <div className="search_item_author">{this.props.book.author}</div>
                    <div className="search_item_price">{this.props.book.price}</div>
                </div>
                <div className="clearB"></div>
                <div className="search_item_action">
                    <a href="#" className="store_btn" onClick={this.addToStore}>Add to Store</a>
                    <a href={this.props.book.link} className="view_btn">View in Amazon</a>
                    <div className="clearB"></div>
                </div>
            </div>
        );
    }
});

var SearchResult=React.createClass({
    getInitialState: function() {
        return {data: SEARCH_LIST, hasLoaded: false};
    },
    componentDidMount: function() {
        SEARCH_TABLE_OBJ=this;
    },
    render: function() {
        if (this.state.hasLoaded) {
            return (
                <div className="admin_search_result">
                    <label htmlFor="admin_search_list">Results</label>
                    <div className="admin_list_wrapper">
                        <ul>
                        {this.state.data.map(function(i,e) {
                            return <SearchItem key={e} book={i}/>
                        })}
                        </ul>
                    </div>
                </div>
            );
        }
        else {
            return null;
        }
    }
});

var SearchBox=React.createClass({
    componentDidMount: function() {
        jQuery('.admin_list_wrapper').enscroll({
            showOnHover: true,
            verticalScrolling: true,
            verticalTrackClass: 'book_scroll_tracker',
            verticalHandleClass: 'book_scroll_handler',
            zIndex: 999,
        });
    },
    render: function() {
        return (
            <div className="book_search_admin">
                <SearchLabel label={this.props.label} />
                <SearchBar search='/admin/scrape/search/'/>
                <SearchResult />
            </div>
        );
    }
});

ReactDOM.render(<BookTable label="Books"/>, document.getElementById('book_container_left'));
ReactDOM.render(<SearchBox label="Search"/>, document.getElementById('book_container_right'));