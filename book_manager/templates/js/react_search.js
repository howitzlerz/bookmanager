var SearchItem=React.createClass({
    render: function() {
        return (
            <li>
                <a href={'/search/book/' + this.props.keyword + '/'} onClick={this.props.select}>{this.props.keyword}</a>
            </li>
        );
    }
});

var SearchBar=React.createClass({
    ajax_server: null,
    getInitialState: function() {
        return ({init_data: [], data: []});
    },
    searchHandler: function(evt) {
        var _keyword=evt.target.value;
        var _this=this;
        if (_keyword.length>=3) {
            _this.ajax_server=jQuery.ajax({
                url: '/service/keyword/search/',
                method: 'POST',
                type: 'POST',
                data: {keyword: _keyword},
                dataType: 'json',
                success: function(ajax_result) {
                    _this.setState({data: ajax_result.result});
                }
            });
        }
        else {
            _this.setState({data: _this.state.init_data});
        }
    },
    componentWillMount: function() {
        var _this=this;
        _this.ajax_server=jQuery.ajax({
            url: '/service/keyword/search/',
            method: 'POST',
            type: 'POST',
            dataType: 'json',
            success: function(ajax_result) {
                _this.setState({data: ajax_result.result, init_data: ajax_result.result});
            }
        });
    },
    componentWillUnmount: function() {
        if (this.ajax_server) {
            this.ajax_server.abort();
        }
        this.ajax_server=null;
    },
    render: function() {
        var _this=this;
        return (
            <div className="book_store_search">
                <div className="book_search_label">Top Searches</div>
                <div className="book_search_box">
                    <input type="text" className="book_search_txt" placeholder="Enter keyword" onKeyUp={this.searchHandler}/>
                    <div className="book_search_btn">Search</div>
                    <div className="clearB"></div>
                </div>
                <div className="book_search_suggestion">
                    <ul>
                        {this.state.data.map(function(i,e) {
                            return <SearchItem keyword={i} key={e}/>
                        })}
                    </ul>
                </div>
            </div>
        );
    }
});

var CartItem=React.createClass({
    render: function() {
        return (
            <div className="collection_cart_item">
                <div className="cart_item_left">
                    <img src={this.props.book.image} />
                </div>
                <div className="cart_item_right">
                    <div className="cart_item_name">{this.props.book.title}</div>
                    <div className="cart_item_line">
                        <input type="text" className="cart_item_qty" value="0"/>
                        <span className="cart_item_price">x {this.props.book.price}</span>
                    </div>
                </div>
                <div class="clearB"></div>
            </div>
        );
    }
});

var Cart=React.createClass({
    getInitialState: function() {
        return ({data: [], total: 0.00});
    },
    render: function() {
        if (this.state.data.length==0) {
            return (
                <div className="collection_cart_container">
                    <div className="collection_cart_label">My cart</div>
                    <div className="collection_cart_list">
                        <div className="collection_cart_empty">
                            <img src="/templates/images/cart_empty.png" />
                            <span>You cart is empty</span>
                        </div>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div className="collection_cart_container">
                    <div className="collection_cart_label">My cart</div>
                    <div className="collection_cart_list">
                    </div>
                    <div className="collection_cart_total">
                        <span className="cart_total_label">Total: </span>
                        <span className="cart_total_value">${this.state.total}</span>
                    </div>
                </div>
            );
        }
    }
});

ReactDOM.render(<SearchBar />, document.getElementById('collection_search'));
ReactDOM.render(<Cart />, document.getElementById('collection_cart'));