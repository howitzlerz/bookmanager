"""book_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'book_store.views.store', name='store'),
    url(r'^admin/$', 'book_store.views.admin', name='admin'),
    url(r'^admin/scrape/search/(?P<keyword>.+)/', 'book_store.views.service_scrape_search', name='admin search book'),
    url(r'^admin/book/add/$', 'book_store.views.service_book_add', name='admin add book'),
    url(r'^admin/book/list/$', 'book_store.views.service_book_list', name='admin list book'),
    url(r'^admin/book/delete/$', 'book_store.views.service_book_delete', name='admin delete book'),
    url(r'^admin/view/(?P<record_type>\w+)/(?P<record_id>.+)/$', 'book_store.views.view_record', name='admin view'),
    url(r'^admin/update/(?P<record_type>\w+)/(?P<record_id>.+)/$', 'book_store.views.update_record', name='admin update'),
    url(r'^admin/save/(?P<record_type>\w+)/(?P<record_id>.+)/$', 'book_store.views.save_record', name='admin save'),
    url(r'^admin/list/(?P<record_type>\w+)/$', 'book_store.views.list_record', name='admin list'),
    url(r'^admin/delete/(?P<record_type>\w+)/(?P<record_id>.+)/$', 'book_store.views.delete_record', name='admin delete'),
    url(r'^service/book/list/(?P<order_type>\w+)/$', 'book_store.views.service_book_list', name='service list book'),
    url(r'^service/keyword/search/$', 'book_store.views.service_keyword_search', name='keyword search'),
    url(r'^search/book/(?P<keyword>.+)/$', 'book_store.views.search_record', name='search record'),
    url(r'^book/(?P<book_id>\w+)/$', 'book_store.views.search_item', name='search item'),
]

#----- Addded code from legacy
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns();
#----- Added code from legacy